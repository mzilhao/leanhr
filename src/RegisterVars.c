
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void LeanHR_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT ierr = 0, group, var, rhs;

  // register evolution and rhs gridfunction groups with MoL

  group = CCTK_GroupIndex("ADMBase::metric");
  ierr += MoLRegisterConstrainedGroup(group);
  group = CCTK_GroupIndex("ADMBase::curv");
  ierr += MoLRegisterConstrainedGroup(group);


  var   = CCTK_VarIndex("ADMBase::alp");
  rhs   = CCTK_VarIndex("LeanHR::rhs_alp");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("ADMBase::betax");
  rhs   = CCTK_VarIndex("LeanHR::rhs_betax");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("ADMBase::betay");
  rhs   = CCTK_VarIndex("LeanHR::rhs_betay");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("ADMBase::betaz");
  rhs   = CCTK_VarIndex("LeanHR::rhs_betaz");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::conf_fac");
  rhs   = CCTK_VarIndex("LeanHR::rhs_conf_fac");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::hxx");
  rhs   = CCTK_VarIndex("LeanHR::rhs_hxx");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::hxy");
  rhs   = CCTK_VarIndex("LeanHR::rhs_hxy");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::hxz");
  rhs   = CCTK_VarIndex("LeanHR::rhs_hxz");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::hyy");
  rhs   = CCTK_VarIndex("LeanHR::rhs_hyy");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::hyz");
  rhs   = CCTK_VarIndex("LeanHR::rhs_hyz");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::hzz");
  rhs   = CCTK_VarIndex("LeanHR::rhs_hzz");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::axx");
  rhs   = CCTK_VarIndex("LeanHR::rhs_axx");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::axy");
  rhs   = CCTK_VarIndex("LeanHR::rhs_axy");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::axz");
  rhs   = CCTK_VarIndex("LeanHR::rhs_axz");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::ayy");
  rhs   = CCTK_VarIndex("LeanHR::rhs_ayy");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::ayz");
  rhs   = CCTK_VarIndex("LeanHR::rhs_ayz");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::azz");
  rhs   = CCTK_VarIndex("LeanHR::rhs_azz");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::tracek");
  rhs   = CCTK_VarIndex("LeanHR::rhs_tracek");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::Theta");
  rhs   = CCTK_VarIndex("LeanHR::rhs_Theta");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::gammatx");
  rhs   = CCTK_VarIndex("LeanHR::rhs_gammatx");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::gammaty");
  rhs   = CCTK_VarIndex("LeanHR::rhs_gammaty");
  ierr += MoLRegisterEvolved(var, rhs);

  var   = CCTK_VarIndex("LeanHR::gammatz");
  rhs   = CCTK_VarIndex("LeanHR::rhs_gammatz");
  ierr += MoLRegisterEvolved(var, rhs);


  if (ierr) CCTK_ERROR("Problems registering with MoL");

}
